# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a system which generates the feature-specific rating of products by performing **sentiment analysis on reviews** in online shopping websites like flipkart and amazon.

### How do I get set up? ###

* Built using Python_Django framework
* NLTK for sentiment analysis
* sentiwordnet dictionary for word polarity scores

### Description

* Product reviews are fetched from flipkart and amazon (around 1000) based on product name
* Each review is processed one by one, by which fuzzy intensity algorithm is used to find polarity of a review. Each sentence of a review is categorized based on the product feature it focuses on(like camera, processor etc). Ratings of all reviews of product are combined to get overall **feature specific rating of the product**.
* A comparative study of other upcoming proximity based approaches like **pattern list and bin list approaches** was done. They were compared based on the accuracy of prediction of polarity ratings.