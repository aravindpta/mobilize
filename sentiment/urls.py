from django.conf.urls import patterns, include, url
from django.contrib import admin
from analyzer import views
admin.autodiscover()
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'sentiment.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', views.index, name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^analyzer/', include('analyzer.urls')), 
 	url(r'^test', views.test, name = 'test'),
 	url(r'^register/$', views.register, name='register'),
 	url(r'^login/$', views.user_login, name='login')
 	
)
