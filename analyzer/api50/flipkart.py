#! /usr/bin/env python
import requests
from purl import URL
import sys
import re
from bs4 import BeautifulSoup
from urllib2 import urlopen,Request
if sys.version_info >= (3, 0):
    from urllib.request import urlopen
else:
    from urllib2 import urlopen


class Flipkart():
    #limit=0	
    def init(self):
        self.base_url = 'http://www.flipkart.com'
	self.limit=0
	self.count=0
	self.items={}

    def search(self,query,category):
  	from extract import Extract
  	e=Extract()
	url=e.geturl(query,category)
	results_page =requests.get(url)
	soup = BeautifulSoup(results_page.text)
        search_results = soup.findAll("div", {"class" : "recentReviews" })
	if len(search_results)==0:
        	search_results = soup.findAll("div", {"class" : "helpfulReviews" })
	for result1 in search_results:
		href1=result1.find('a')['href']
		url1 = href1
	
	#print url1
	return {'reviewpage':'http://www.flipkart.com'+url1,'productpage':url}
	
   
    def iterate(self,url):
	results_page = requests.get(url)
	print "hello"			
        soup = BeautifulSoup(results_page.text)
        search_results = soup.findAll("span", {"class" : "review-text" })
	for result in search_results:
		#print result
		result = re.sub('[ \t]+' , ' ', result.get_text())
		#print "Review no:"+str(self.count)
		#print result
		self.items[self.count]=result
		self.count=self.count+1
		#print "hello"
	
	while(self.limit<3):
		search_results = soup.findAll("a", {"class" : "nav_bar_next_prev" })
		if len(search_results)==0:
			break
		for result1 in search_results:
			href1=result1['href']
			url1 = 'http://www.flipkart.com'+href1
		self.limit=self.limit+1
		self.iterate(url1)
		
	
	return self.items
   
