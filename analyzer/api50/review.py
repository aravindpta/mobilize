###! /usr/bin/env python
import requests
from purl import URL
import sys
import re
from bs4 import BeautifulSoup

if sys.version_info >= (3, 0):
    from urllib.request import urlopen
else:
    from urllib2 import urlopen



class Flipkart():

    def init(self):
	
        self.base_url = 'http://www.flipkart.com'
	

    def search(self,query):
	from extract import Extract
  	e=Extract()
	url=e.geturl()
	#print url
	results_page = requests.get(url)
	#print results_page			
        soup = BeautifulSoup(results_page.text)
        search_results = soup.findAll("div", {"class" : "recentReviews" })
	if len(search_results)==0:
        	search_results = soup.findAll("div", {"class" : "helpfulReviews" })
	print search_results
	if len(search_results)==0:
		return 0
	for result1 in search_results:
		href1=result1.find("a")['href']
		print href1
		url1 = 'http://www.flipkart.com' + href1
		#print url1
	results_page = requests.get(url1)
	soup = BeautifulSoup(results_page.text)
        search_results = soup.findAll("span", {"class" : "review-text" })
	return search_results

    def list_items(self, search_results):
        items= {} 
	i=0
        for result in search_results:
	    result = re.sub('[ \t]+' , ' ', result.get_text())
            items[i]=result
	    i=i+1
        return items
