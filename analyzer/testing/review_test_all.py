import numpy
import itertools
import datetime
import math
import json
from sklearn.svm import LinearSVC,SVC
import proximity_tagger
import re
import os
import sys
lib = os.path.abspath('analyzer/testing')
sys.path.append(lib)
from postag1 import postg
from nltk.corpus import phone_review
lib = os.path.abspath('analyzer/matspeed')
sys.path.append(lib)
from sentiwordnet1 import SentiWordNetCorpusReader

def median_result(file_to_test,isphrase):

        median_testset=[]
        f = open('proximity_median_train_result_'+str(isphrase),'r')
        median_testset=json.load(f)
        f.close()
        median_test = proximity_tagger.medianlist(file_to_test,isphrase)
        #print "hai"
        med_pos_val = median_testset[0][0]-median_test[0]                                    
        med_neg_val = median_testset[1][1]-median_test[1]

	if(med_pos_val<med_neg_val):
            return 1
        return -1

def review_test(isphrase,llimit,ulimit,approach,swn):
	senti=SentiWordNetCorpusReader('SentiWordNet.txt')
	lib = os.path.abspath('analyzer/testing')
	sys.path.append(lib)
	print sys.path
	poscount=0
	poscountall=[]
	totalcount=int(ulimit)-int(llimit)
	lpcount=0
	print llimit,ulimit
	p=postg()
	from nltk.corpus import phone_review
	clf=LinearSVC()
	clf2=LinearSVC()
	print '\nNo of +ve reviews scanned for training :'
	for fid in phone_review.fileids(categories=['pos'])[int(llimit):int(ulimit)]:
		
			bin_testset=[]
			f = open('analyzer/testing/binresult.txt','r')
			bin_testset=json.load(f)
			f.close()
	
			#clf=SVC()
			
			clf.fit(bin_testset[0],bin_testset[1])
			count=0
			cnt_var=0
			bin_val=-1
			temp_class1= clf.predict(proximity_tagger.bin_list(phone_review.abspath(fid),isphrase,swn))
			lpcount=lpcount+1
        		cnt_var+=1
			isreviewpositive=0
			print 'Scanning +ve review ',lpcount,'.'*10,(float(lpcount)*100/float(totalcount)),'%'
			if temp_class1 == [1]:
				bin_val=1
			if (bin_val)>0:
				isreviewpositive = 1
				poscount=poscount+1
			print isreviewpositive
	final=(float(poscount)*100/float(totalcount))
	poscountall.append(final)

	poscount=0
	lpcount=0
	for fid in phone_review.fileids(categories=['pos'])[int(llimit):int(ulimit)]:
			pattern_testset=[]
			f = open('analyzer/testing/patternresult.txt','r')
			pattern_testset=json.load(f)
			f.close()

			#clf2=SVC()
			
			clf2.fit(pattern_testset[0],pattern_testset[1])

			count=0
			cnt_var=0
			pat_val=-1
		
			#med_val = median_result('samplereview.txt',isphrase)
	
			temp_class2= clf2.predict(proximity_tagger.pattern_list(phone_review.abspath(fid),isphrase,swn))
			lpcount=lpcount+1
        		cnt_var+=1
        		isreviewpositive=0
			print 'Scanning +ve review ',lpcount,'.'*10,(float(lpcount)*100/float(totalcount)),'%'
			numsum=sum(temp_class2)
        		if numsum >=(len(temp_class2)/2):
				pat_val=1
			if (pat_val)>0:
				isreviewpositive = 1
				poscount=poscount+1
	final=(float(poscount)*100/float(totalcount))
	poscountall.append(final)

	poscount=0
	lpcount=0
	for fid in phone_review.fileids(categories=['pos'])[int(llimit):int(ulimit)]:
			fo = open(phone_review.abspath(fid))
    			string = fo.read();
			string=re.split('\.|[b][u][t]|[w][h][i][l][e]|[t][h][o][u][g][h]|[\n]',string)
			value=p.classify(string,senti)
			if value==1:
				poscount=poscount+1
	final=(float(poscount)*100/float(totalcount))
	poscountall.append(final)

	print poscountall
	return poscountall
