from django.conf.urls import patterns, url
from analyzer import views
#admin.autodiscover()
urlpatterns = patterns('',url(r'^$', views.index, name='index'),
	                  url(r'^about',views.about,name='about'),
	                      url(r'^test', views.test, name = 'test'),
	                       url(r'^progress', views.progress, name = 'progress'),
	                       url(r'^review', views.review, name = 'review'),
	                       url(r'^search', views.search, name = 'search'),
	                       url(r'^comparison', views.comparison, name = 'comparison'),
	                       url(r'^study', views.study, name = 'study'),
	                       url(r'^register/$', views.register, name='register'),
	                       url(r'^login/$', views.user_login, name='login'),
	                       url(r'^dummy/$', views.dummy, name='dummy'),
	                       url(r'^logout/$','django.contrib.auth.views.logout',{'next_page': '/analyzer/'}),
	                       url(r'^history/$', views.history, name='history'),
	                       url(r'^all/$', views.all, name='all'))
