import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
import re
class sentdiv:
	def divfunc(self, text):
		sentence=[]
		overall = []
		sound=[]
		camera=[]
		display=[]
		battery=[]
		design=[]
		processor=[]
		sound=[]
		gaming=[]
		user_experience=[]
		multitask=[]
		memory=[]
		#text = ("This is where Microsoft got everything right!This phone has got the flash at the rear end, front facing 5MP wide angle, 5 inch screen.Battery power 1905mAh. Trust me this is more than enough for a phone running a WP8.1. battery life is similar to an Android phone having a 2200mAh battery.1GB ram. That is a great upgradation.These three major features (Missing in 52X series) are added. Additionally, this phone comes with a Denim update.Gorilla glass 3 is great! Scratch resistant.Cool right!.At this price you can compare with any other lower priced smartphone. Beats everything hands down.What fluidity man! The OS is really smooth and doesn't lag while running heavy applications.This is going to be a killer. If you can afford more, get a Lumia 730. ")
		text=re.split('\.|[b][u][t]|[w][h][i][l][e]|[t][h][o][u][g][h]||[,][\n]',text)
		#print text
		#tags=nltk.pos_tag(text)
		for i in text:
			i=i.strip()
			print i
			sentence.append(i)
			print '\n'
		#print text
		patterns = [
     		(r'(?i).*[cC]amera[s]?.*|.*[pP]hoto[s]?.*|.*[fF]lash[s]?.*', 'CA'),               # gerunds
		    (r'.*[dD]isplay[s]?.*|.*[sS]creen[s]?.*|.*[gG]orilla.*|.*[tT]ouch[s]?.*','DI'),
     		(r'.*[bB]attery[s]?.*|.*[cC]harge[s]?.*','BA'),
     		(r'.*[Dd]esign[s]?.*|.*[bB]uild[s]?.*|.*[wW]eigh[t]?[s]?.*','DE'),
     		(r'.*[pP]rocessor[s]?.*', 'PR'),               # gerunds
     		(r'.*[sS]ound[s]?.*|.*[sS]peaker[s]?.*|.*[hH]ead\s?phone[s]?.*|.*\s[eE]ar[s]?.*','SO'),
     		(r'.*[gG]aming[s]?.*|.*[gG]ame[s]?.*','GA'),
     		(r'.*[uU]ser experience[s]?.*|.*[U][iI][s]?.*|.*[iI]nterface[s]?.*','UE'),
     		(r'.*[mM]ultitask[s]?.*','MU'),
     		(r'.*[mM]emory[s]?.*|.*[ ][rR][ao]m[s]?.*','ME'),
  		]


		regexp_tagger = nltk.RegexpTagger(patterns)
		tags=regexp_tagger.tag(sentence)
		temp=None

		for i in tags:
			print i
			if (i[1]=='CA' or i[1]=='DI' or i[1]=='BA' or i[1]=='DE' or i[1]=='PR' or i[1]=='SO' or i[1]=='GA' or i[1]=='UE' or i[1]=='MU'or i[1]=='ME'):
		
				if i[1]=='CA':
					temp="camera"
					camera.append(i[0])
				if i[1]=='DI':
					temp="display"
					display.append(i[0])
				if i[1]=='BA':
					temp="battery"
					battery.append(i[0])
				if i[1]=='DE':
					temp="design"
					design.append(i[0])
				if i[1]=='PR':
					temp="processor"
					processor.append(i[0])
				if i[1]=='SO':
					temp="sound"
					sound.append(i[0])
				if i[1]=='GA':
					temp="gaming"
					gaming.append(i[0])
				if i[1]=='UE':
					temp="user experience"
					user_experience.append(i[0])
				if i[1]=='MU':
					temp="multitasking"
					multitask.append(i[0])
				if i[1]=='ME':
					temp="memory"
					memory.append(i[0])

			else:
				if not temp:
					overall.append(i[0])
				elif temp=="camera":
					camera.append(i[0])
				elif temp=="display":
					display.append(i[0])
				elif temp=="battery":
					battery.append(i[0])
				elif temp=="design":
					design.append(i[0])
				elif temp=="processor":
					processor.append(i[0])
				elif temp=="sound":
					sound.append(i[0])
				elif temp=="gaming":
					gaming.append(i[0])
				elif temp=="user experience":
					user_experience.append(i[0])
				elif temp=="multitasking":
					multitask.append(i[0])
				elif temp=="memory":
					memory.append(i[0])
			
		'''
		print '\nCamera:\n'
		print camera

		print '\nDisplay:\n'
		print display

		print '\nBattery:\n'
		print battery

		print '\nDesign:\n'
		print design

		print '\nProcessor:\n'
		print processor

		print '\nSound:\n'
		print sound

		print '\nGaming:\n'
		print gaming

		print '\nUser Experience:\n'
		print user_experience

		print '\nMultitasking:\n'
		print multitask

		print '\nMemory:\n'
		print memory'''

		array = {}
		array["camera"] = camera
		array["battery"] = battery
		array["display"] = display
		array["design"] = design
		array["processor"] = processor
		array["sound"] = sound
		array["gaming"] = gaming
		array["user_experience"] = user_experience
		array["multitask"] = multitask
		array["memory"] = memory
		array["overall"] = overall
		'''array[camera] = camera
		array[design] = design
		array[processor] = processor
		array[sound] = sound
		array[gaming] = gaming
		array[experience] = user_experience
		array[multitask] = multitask
		array[memory] = memory'''
		print array
		return array
