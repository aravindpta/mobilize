from django.db import models
from django.contrib.auth.models import User

class History(models.Model):
	name = models.ForeignKey(User)
	pname = models.CharField(max_length=128)
	score = models.IntegerField(default=0)
	def __unicode__(self):
		return str(self.name)
# Create your models here.
