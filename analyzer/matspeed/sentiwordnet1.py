#!/usr/bin/env python

"""
Interface to SentiWordNet using the NLTK WordNet classes.

---Chris Potts
"""
import psycopg2
import re
import os
import sys
import codecs
import nltk


try:
    from nltk.corpus import wordnet as wn
except ImportError:
    sys.stderr.write("Couldn't find an NLTK installation. To get it: http://www.nltk.org/.\n")
    sys.exit(2)

######################################################################

class SentiWordNetCorpusReader:
    def __init__(self, filename):
        """
        Argument:
        filename -- the name of the text file containing the
                    SentiWordNet database
        """        
        self.filename = filename
        print filename
        self.db = {}
        self.parse_src_file()

    def parse_src_file(self):
        print "hai"
        conn = psycopg2.connect(database="sentiment", user="aravind", password="aravind", host="127.0.0.1", port="5432")
        cur = conn.cursor()
        cur.execute("SELECT *  from DICTIONARY;")
        rows = cur.fetchall()
        for row in rows:
            offset=row[0]
            pos=row[1]
            pos_score=row[2]
            neg_score=row[3]
            synset_terms=row[4]
    
            if pos and offset:
                offset = int(offset)
                self.db[(pos, offset)] = (float(pos_score), float(neg_score))
                #print self.db
        conn.commit()
        conn.close()
        print "hai"

    def senti_synset(self, *vals): 
        #print vals 
        if tuple(vals) in self.db:
            pos_score, neg_score = self.db[tuple(vals)]
            pos, offset = vals
            synset = wn._synset_from_pos_and_offset(pos, offset)
            return SentiSynset(pos_score, neg_score, synset)
        else:
            #print vals
            synset = wn.synset(vals[0])
            pos = synset.pos()
            if pos=='s':
		pos='a'
            offset = synset.offset()
            print offset
            if (pos, offset) in self.db:
                pos_score, neg_score = self.db[(pos, offset)]
                #print "hello"
                #print pos_score
                return SentiSynset(pos_score, neg_score, synset)
            else:
                print "none"
                return SentiSynset(0, 0, "no_synset")

    def details(self, string):
	print "string:"+string[0]
	
	pos=string[1]
	print pos
        print "enter"
        if 'NN' in pos:
            pos='n'
        elif 'JJ' in pos:
            pos='a'
        elif 'RB' in pos:
            pos='r'
        elif 'VB' in pos:
            pos='v'
        else:
            pos=None
        sentis = []
        synset_list = wn.synsets(string[0],pos)
        print synset_list
        for synset in synset_list:
            if string[0][0]==synset.name()[0]:
                    print synset.name()
            sentis.append(self.senti_synset(synset.name()))
        sentis = filter(lambda x : x, sentis)
	if sentis==[]:
		#temp=
		sentis.append(SentiSynset(0, 0, "no_synset"))
        #print sentis
	if not(sentis[0].pos_score==0 and sentis[0].neg_score==0): 
                    return sentis
        pos='a'
	sentis = []
        synset_list = wn.synsets(string[0],pos)
        #print synset_list
        for synset in synset_list:
            if string[0][0]==synset.name()[0]:
                    print synset.name()
                    sentis.append(self.senti_synset(synset.name()))
        sentis = filter(lambda x : x, sentis)
	if sentis==[]:
		#temp=
		sentis.append(SentiSynset(0, 0, "no_synset"))
	return sentis

    def all_senti_synsets(self):
        for key, fields in self.db.iteritems():
            pos, offset = key
            pos_score, neg_score = fields
            synset = wn._synset_from_pos_and_offset(pos, offset)
            yield SentiSynset(pos_score, neg_score, synset)

######################################################################
            
class SentiSynset:
    def __init__(self, pos_score, neg_score, synset):
        self.pos_score = pos_score
        self.neg_score = neg_score
        self.obj_score = 1.0 - (self.pos_score + self.neg_score)
        self.synset = synset

    def __str__(self):
        """Prints just the Pos/Neg scores for now."""
        s = ""
        s += self.synset.name + "\t"
        s += "PosScore: %s\t" % self.pos_score
        s += "NegScore: %s" % self.neg_score
        return s

    def __repr__(self):
        return "Senti" + repr(self.synset)
                    
######################################################################        

'''if __name__ == "__main__":
    """
    If run as

    python sentiwordnet.py

    and the file is in this directory, send all of the SentiSynSet
    name, pos_score, neg_score trios to standard output.
    """
    SWN_FILENAME = "SentiWordNet.txt"
    if os.path.exists(SWN_FILENAME):
        swn = SentiWordNet(SWN_FILENAME)
        #for senti_synset in swn.all_senti_synsets():
            #print senti_synset.synset.name, senti_synset.pos_score, senti_synset.neg_score'''
