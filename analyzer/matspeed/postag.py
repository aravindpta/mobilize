import nltk, math
from itertools import islice
from sentiwordnet1 import SentiWordNetCorpusReader,SentiSynset
swn_filename ='SentiWordNet.txt'
s = SentiWordNetCorpusReader(swn_filename)

class postg(object):
	"""docstring for postg"""
	def intensify(self, details0,details1):
		try:
			pos1=details1.pos_score
			neg1=details1.neg_score
			pos0=details0.pos_score
			neg0=details0.neg_score
		except:
			pos1=details1[0]
			neg1=details1[1]
			pos0=details0.pos_score
			neg0=details0.neg_score

		if neg0>pos0:
			flag=1
			pos1=1-pos1
			neg1=1-neg1
		else:
			if pos1>=.5:
				pos1=math.sqrt(pos1)
			else:
				pos1=math.sqrt(pos1)
			if neg1>= .5:
				neg1=math.sqrt(neg1) 
			else:
				neg1=math.sqrt(neg1)

		details=pos1,neg1
        	print details
        	return details

	def classify(self, pname):
		
		tokens = nltk.word_tokenize(pname)
		tagged = nltk.pos_tag(tokens)
		wordlist=[]
		for tag in tagged :           
			if not (tag[1]=='IN' or tag[1]=='CC' or tag[1]=='CD'\
				or tag[1]=='DT' or tag[1]=='TO' or tag[1]==','\
					or tag[1]==';' or tag[1]==':'):
					wordlist.append(tag)
		i=0	
		result=0
		flag = 0
		while i<(len(wordlist)):
			print "i"+str(i)
			#flag=0
			score=0
			temp0 = s.details(wordlist[i])
			if temp0!=[]:
				temp0=temp0[0]
				print wordlist[i][1]
				#print wordlist[i+1][1]
				#print temp0
				if (wordlist[i][1] == 'RB' and (i+1)<len(wordlist) and wordlist[i+1][1]== 'JJ'):
					temp1=s.details(wordlist[i+1])[0]
					print "first if"
					#print wordlist[i+1][0]
					#print temp1
					details=self.intensify(temp0,temp1)
					print details[1]
					if details[1]>details[0]:
						score=0-details[1]
        				else:
        					score=details[0]
        				i=i+1

				elif (wordlist[i][1] == 'JJ' and (i+1)<len(wordlist) and wordlist[i+1][1]== 'NN'):
					temp1=s.details(wordlist[i+1])[0]
					print "second if"
					if (temp1.pos_score==0 and temp1.neg_score==0):
						details=temp0.pos_score,temp0.neg_score
					else:
						details=self.intensify(temp0,temp1)
					if details[1]>details[0]:
						score=0-details[1]
        				else:
        					score=details[0]
        				i=i+1

				elif (wordlist[i][1] == 'RB' and (i+2)<len(wordlist) and wordlist[i+1][1]== 'RB' and (wordlist[i+2][1]=='JJ'or wordlist[i+2][1]=='JJ')):
					print "third if"
        				temp2=s.details(wordlist[i+2])[0]
					temp1=s.details(wordlist[i+1])[0]
            				details=self.intensify(temp1,temp2)
					details=self.intensify(temp0,details)
				
					if details[1]>details[0]:
            					score=0-details[1]
        				else:
            					score=details[0]

                			i=i+2

				elif temp0!=None:
					#print temp0[1]
					if temp0.neg_score>temp0.pos_score:
            					score=0-temp0.neg_score
        				else:
            					score=temp0.pos_score
			print "intermediate"+str(result)
			result = result + score
			print result
			i=i+1

			#print result	
		if result >= 0:
			print 'result is positive'
		else:
			print 'result is negative'
		print 'result = ', result
