import nltk
import re
from sentiwordnet1 import SentiWordNetCorpusReader
from textblob import TextBlob
from textblob_aptagger import PerceptronTagger

class postg(object):
	"""docstring for postg"""
	def classify(self, review,s):
		count=0
		score=0
		average=0
		for sent in review:
			tokens = nltk.word_tokenize(sent)
			tagged = nltk.pos_tag(tokens)
			wordlist=[]
			for tag in tagged :
				flg = 0           
				if not (tag[1]=='IN' or tag[1]=='CC' or tag[1]=='CD'\
					or tag[1]=='DT' or tag[1]=='TO'):
					flg = 1
			
					wordlist.append(tag)

			i=0
			posscore=0
			negscore=0
			flag = 0
			f =0
			flag1 = 0
			for word in wordlist:
				print word
				if word[1] == 'RB' or 'VB' in word[1]:
					temp = s.details(word)[0]
					try:
						if temp.pos_score<temp.neg_score :
							flag1 = 1
							print('compliment')
					except:
						f = 1
				elif word[0] == ',' or word[0] == ';' or word[0] == ':':
					flag = 0
				temp = {}
				temp = s.details(word)[0]
				if not(temp is None):
					if not flag:
						posscore = posscore + temp.pos_score
						negscore = negscore + temp.neg_score
					else:
						posscore = posscore + temp.neg_score
						negscore = negscore + temp.pos_score
				i=i+1
				if flag1:
					flag = 1
				print 'intermediate result = ', posscore, '------', negscore, '------', flag
			result = posscore - negscore
			if result == 0:
				print 'result is neutral'
			elif result > 0:
				count=count+1
				score=score+1
				print 'result is positive'
			else:
				#score=score-1
				count=count+1
				print 'result is negative'
			print 'result = ', result, 'pos = ', posscore, 'neg = ', negscore
		if count==0:
			return None
		average=score/float(count)
		print 'average=' ,average, 'score=', score
		if average>.4:
			return 1
		else:
			return 0
		
