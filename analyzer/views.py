from django.shortcuts import render
from django.shortcuts import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.contrib.auth import authenticate, login
from django.template import RequestContext
from django.shortcuts import render_to_response
from analyzer.forms import UserForm
from django.http import HttpResponse, HttpResponseRedirect
from django import forms
import os
import sys
import re
lib = os.path.abspath('analyzer/api50')
sys.path.append(lib)
from usage import extractor
from subject import sentdiv
lib = os.path.abspath('analyzer/matspeed')
sys.path.append(lib)
from postag1 import postg
from postgcustom import postgc
from sentiwordnet1 import SentiWordNetCorpusReader
lib = os.path.abspath('analyzer/amapi')
sys.path.append(lib)
from usage1 import aextractor
lib = os.path.abspath('analyzer/testing')
sys.path.append(lib)
from proximity_analysis import proximity
from sentiwordnet import SentiWordNetCorpusReader as sentinet

def index(request):
     # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    context_dict = {'boldmessage': "I am bold font from the context"}

    # Return a rendered response to send to the client.
    # We make use of the shortcut function to make our lives easier.
    # Note that the first parameter is the template we wish to use.

    return render(request, 'analyzer/index.html')


def history(request):
     # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    from analyzer.models import History
    p=History.objects.filter(name=request.user)
    p={'p':p}
    return render(request, 'analyzer/history.html',p)

def register(request):
    # Like before, get the request's context.
    context = RequestContext(request)

    # A boolean value for telling the template whether the registration was successful.
    # Set to False initially. Code changes value to True when registration succeeds.
    registered = False

    # If it's a HTTP POST, we're interested in processing form data.
    if request.method == 'POST':
        # Attempt to grab information from the raw form information.
        # Note that we make use of both UserForm and UserProfileForm.
        user_form = UserForm(data=request.POST)
        #profile_form = UserProfileForm(data=request.POST)

        # If the two forms are valid...
        if user_form.is_valid():
            # Save the user's form data to the database.
            user = user_form.save()

            # Now we hash the password with the set_password method.
            # Once hashed, we can update the user object.
            user.set_password(user.password)
            user.save()

            # Now sort out the UserProfile instance.
            # Since we need to set the user attribute ourselves, we set commit=False.
            # This delays saving the model until we're ready to avoid integrity problems.
            ''' profile = profile_form.save(commit=False)
            profile.user = user'''

            # Did the user provide a profile picture?
            # If so, we need to get it from the input form and put it in the UserProfile model.
            '''if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']'''

            # Now we save the UserProfile model instance.
            

            # Update our variable to tell the template registration was successful.
            registered = True

        # Invalid form or forms - mistakes or something else?
        # Print problems to the terminal.
        # They'll also be shown to the user.
        else:
            print user_form.errors

    # Not a HTTP POST, so we render our form using two ModelForm instances.
    # These forms will be blank, ready for user input.
    else:
        user_form = UserForm()
        
    # Render the template depending on the context.
    return render_to_response(
            'analyzer/register.html',
            {'user_form': user_form,'registered': registered},
            context)

def user_login(request):
    # Like before, obtain the context for the user's request.
    context = RequestContext(request)

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
        username = request.POST['username']
        password = request.POST['password']

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user:
            # Is the account active? It could have been disabled.
            if user.is_active:
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)
                return HttpResponseRedirect('/analyzer/')
            else:
                # An inactive account was used - no logging in!
                return HttpResponse("Your Rango account is disabled.")
        else:
            # Bad login details were provided. So we can't log the user in.
            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")

    # The request is not a HTTP POST, so display the login form.
    # This scenario would most likely be a HTTP GET.
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render_to_response('analyzer/login.html', {}, context)

def dummy(request):
     # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    
    
    return render(request, 'analyzer/dummy.html')


def progress(request):
     # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    return render(request, 'analyzer/progress.html')

def search(request):
     # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    review = request.GET.get('customreview', '')
    review=re.split('\.|[b][u][t]|[w][h][i][l][e]|[t][h][o][u][g][h]|[\n]',review)
    senti=SentiWordNetCorpusReader('SentiWordNet.txt')
    p=postgc()
    result=p.classify(review,senti)
    if result==0:
        value="neutral"
    elif result>0:
        value="positive"
    else:
        value="negative"
    final={'result':result,'value':value}

    return render(request, 'analyzer/search.html',final)

def review(request):
     # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    return render(request, 'analyzer/review.html')

def all(request):
     # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    return render(request, 'analyzer/all.html')

def study(request):
     # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    approach = request.GET.get('approach', '')
    ulimit=request.GET.get('ulimit','')
    llimit=request.GET.get('llimit','')
    swn_filename ='SentiWordNet.txt'
    swn = sentinet()
    #synset=SentiSynset()
    p=proximity()
    value=p.test(llimit,ulimit,approach,swn)
    if approach=='all':
        result={'value':value,'approach':approach}
        return render(request, 'analyzer/all.html',result)
    else:
        result={'result':value}
        return render(request, 'analyzer/study.html',result)


def comparison(request):
     # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    
    
    return render(request, 'analyzer/comparison.html')

def about(request):
     # Construct a dictionary to pass to the template engine as its context.
    # Note the key boldmessage is the same as {{ boldmessage }} in the template!
    return HttpResponse("Analyzer says here is the about page")

def test(request):
    from analyzer.models import History
    from analyzer.models import User
    param = request.GET.get('search', '')
    select=request.GET.get('select','')
    site=request.GET.get('site','')
    category=request.GET.get('category','')
    print site
    #return HttpResponse(param)
    e = extractor()
    a=aextractor()
    s = sentdiv()
    p=postg()
    if site=='flipkart':
        print "flipkart"
        temp= e.extrev(param,category)
    else:
        temp=a.extrev(param)
    string=temp.get('result')
    url=temp.get('url')
    print string
    if select=='reviews':
        print string
        listall={'message':string}
        return render(request, 'analyzer/review.html', listall)
    senti=SentiWordNetCorpusReader('SentiWordNet.txt')
    
    divide = {}
    result={}
    totalrate=0
    totalcount=0
    result["camera"] = 0
    result["battery"] = 0
    result["display"] = 0
    result["design"] = 0
    result["processor"] = 0
    result["sound"] = 0
    result["gaming"] = 0
    result["user_experience"] = 0
    result["multitask"] = 0
    result["memory"] = 0
    result["overall"] = 0
    result["final"]=0
    count={}
    count["camera"] = 0
    count["battery"] = 0
    count["display"] = 0
    count["design"] = 0
    count["processor"] = 0
    count["sound"] = 0
    count["gaming"] = 0
    count["user_experience"] = 0
    count["multitask"] = 0
    count["memory"] = 0
    count["overall"] = 0
    count["final"]=0
    j=0
    for i, substr in string:
        divide[j] = s.divfunc(substr)
        for feature,text in divide[j].items():
            if len(text)==0:
                continue
            newresult=p.classify(text,senti)
            if newresult!=None:
                result[feature]=result[feature]+newresult #separate count for polarity
                result["final"]=result["final"]+newresult
                count[feature]=count[feature]+1 #total count for average
                count["final"]=count["final"]+1
        j=j+1
    #print divide
    print result
    
    for feature,value in count.items():
        print 'feature=',feature,'count=',count[feature]

    for feature,value in result.items():
        if count[feature]!=0:
            result[feature]=float(result[feature])/count[feature]
        else:
            result[feature]="-1"

    print result
    value=result['final']
    listall = {'message':string, 'dividedstr':divide}
    
    result={'result':result,'url':url,'pname':param}
    print listall
    print request.user
    print value
    '''p = History(name=request.user, pname=param, score=value)
    p.save()'''
    try:
        History.objects.create(name=request.user, pname=param, score=value*100)
        p=History.objects.filter(name=request.user)
        print p[1].pname
        return render(request, 'analyzer/progress.html', result)
    except:
        return render(request, 'analyzer/progress.html', result)
    #return HttpResponse(string)