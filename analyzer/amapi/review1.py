#! /usr/bin/env python
import requests
from purl import URL
import sys
import re
from bs4 import BeautifulSoup
from urllib2 import urlopen,Request
if sys.version_info >= (3, 0):
    from urllib.request import urlopen
else:
    from urllib2 import urlopen


class Flipkart():
    #limit=0	
    def init(self):
        self.base_url = 'http://www.flipkart.com'
        self.limit=0
        self.count=0
        self.items={}

    def search(self,query):
  		from extract1 import Extract
  		e=Extract()
		url=e.geturl(query)
		results_page =requests.get(url)
		soup = BeautifulSoup(results_page.text)
		search_results = soup.findAll("a", {"class" : "a-link-emphasis a-nowrap" })
		if len(search_results)==0:
			search_results = soup.findAll("div", {"class" : "helpfulReviews" })
		for result1 in search_results:
			href1=result1['href']
			url1 = href1
	
	#print url1
		return {'reviewpage':url1,'productpage':url}
	
   
    def iterate(self,url):
		results_page = requests.get(url)
		print "hello"	
		print url		
		#print results_page
		soup = BeautifulSoup(results_page.text)
		#print soup
		search_results = soup.findAll("div", {"class" : "reviewText" })
		for result in search_results:
			#print result
			result = re.sub('[ \t]+' , ' ', result.get_text())
			#print "Review no:"+str(self.count)
			#print result
			self.items[self.count]=result
			self.count=self.count+1
			#print "hello"
		
		while(self.limit<3):
			search_results = soup.findAll("span", {"class" : "paging" })
			if len(search_results)==0:
				break
			search_results=search_results[1].findAll('a')
			for result1 in search_results:
				print result1
				if 'Next' in result1.text:
					href1=result1['href']
					nexturl = href1
					break
			self.limit=self.limit+1
			try:
				self.iterate(nexturl)
			except:
				break
	
		return self.items
   
