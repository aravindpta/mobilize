###! /usr/bin/env python
import requests
from purl import URL
import sys
import re
from bs4 import BeautifulSoup

if sys.version_info >= (3, 0):
    from urllib.request import urlopen
else:
    from urllib2 import urlopen



class Flipkart():

    def init(self):
	
        self.base_url = 'http://www.flipkart.com'
	

    def search(self,query):
	from extract import Extract
  	e=Extract()
	f=0
	url=e.geturl()
	
	#print url
	results_page = requests.get(url)
	#print results_page			
        soup = BeautifulSoup(results_page.text)
        search_results = soup.findAll("a", {"class" : "a-link-emphasis a-nowrap" })
	#print search_results
	if len(search_results)==0:
		return 0
	for result1 in search_results:
		href1=result1['href']
		print href1
		url1 = href1
		#print url1
	results_page = urlopen(url1)
	soup = BeautifulSoup(results_page)
        search_results = soup.findAll("div", {"class" : "reviewText" })
	return search_results

    def list_items(self, search_results):
        items= {} 
	i=0
        for result in search_results:
	    result = re.sub('[ \t]+' , ' ', result.get_text())
            items[i]=result
	    i=i+1
        return items
